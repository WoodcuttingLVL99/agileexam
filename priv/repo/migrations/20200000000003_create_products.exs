defmodule App.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :quantity, :integer
      add :votes, :integer
      add :average, :float
      timestamps()
    end
  end
end
