defmodule App.Repo.Migrations.CreateRatings do
  use Ecto.Migration

  def change do
    create table(:ratings) do
      add :email, :string
      add :score, :integer
      add :product_id, references(:products)
      timestamps()
    end
  end
end
