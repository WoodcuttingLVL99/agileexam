# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Parking.Repo.insert!(%Parking.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias App.{Repo, Accounts.User, App.Product}

[%{name: "Tiit Kääpa", email: "tiit@desperado.com", password: "parool", age: 34},
 %{name: "Teet Kääpa", email: "teet@desperado.com", password: "parool", age: 34}]
|> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)


product = Repo.insert!(%Product{name: "Ham Sandwich", quantity: 5, votes: 0})
product2 = Repo.insert!(%Product{name: "Volvo Car", quantity: 5, votes: 0})

"""
parking_lot = Ecto.build_assoc(zone, :parking_lot, %ParkingLot{name: "Juhan Liivi parking", address: "Juhan Liivi 2, 50409, Tartu"}) |> Repo.insert!()
parking_lot2 = Ecto.build_assoc(zone2, :parking_lot, %ParkingLot{name: "Jaama parking", address: "Jaama 140, 50705, Tartu"}) |> Repo.insert!()
parking_lot3 = Ecto.build_assoc(zone, :parking_lot, %ParkingLot{name: "Kaubamaja parking", address: "Uueturu, 51004 Tartu"}) |> Repo.insert!()
parking_lot4 = Ecto.build_assoc(zone2, :parking_lot, %ParkingLot{name: "Aura parking", address: "Turu 10, 51013, Tartu"}) |> Repo.insert!()
parking_lot5 = Ecto.build_assoc(zone, :parking_lot, %ParkingLot{name: "Lõunakeskus parking", address: "Ringtee 75, 50501, Tartu"}) |> Repo.insert!()
# Note: Linnamuusuem parking is not displayed because all the spaces are occupied
parking_lot6 = Ecto.build_assoc(zone, :parking_lot, %ParkingLot{name: "Linnamuuseum parking", address: "Narva maantee 23, 51009, Tartu"}) |> Repo.insert!()
parking_lot7 = Ecto.build_assoc(zone, :parking_lot, %ParkingLot{name: "Tartu SS Kalev parking", address: "Turu 8, 51013, Tartu"}) |> Repo.insert!()

[%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false}]
|> Enum.map(fn parking_space -> Ecto.build_assoc(parking_lot, :parking_space, parking_space) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false}]
|> Enum.map(fn parking_space -> Ecto.build_assoc(parking_lot2, :parking_space, parking_space) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false}]
|> Enum.map(fn parking_space -> Ecto.build_assoc(parking_lot3, :parking_space, parking_space) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false}]
|> Enum.map(fn parking_space -> Ecto.build_assoc(parking_lot4, :parking_space, parking_space) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false}]
|> Enum.map(fn parking_space -> Ecto.build_assoc(parking_lot5, :parking_space, parking_space) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%ParkingSpace{occupied: true},
%ParkingSpace{occupied: true},
%ParkingSpace{occupied: true}]
|> Enum.map(fn parking_space -> Ecto.build_assoc(parking_lot6, :parking_space, parking_space) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false},
%ParkingSpace{occupied: false}]
|> Enum.map(fn parking_space -> Ecto.build_assoc(parking_lot7, :parking_space, parking_space) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
"""
