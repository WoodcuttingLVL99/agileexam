defmodule AppWeb.ProductControllerTest do
  use AppWeb.ConnCase

  alias App.{Repo, Accounts.User, App.Product, App.Rating}
  alias App.Guardian
  import Ecto.Changeset, only: [change: 2]

  setup do
    product = Repo.insert!(%Product{name: "Hams Sandwich", quantity: 5, votes: 0})
    product2 = Repo.insert!(%Product{name: "Volvo Car", quantity: 5, votes: 0})

    conn = build_conn()
    {:ok, conn: conn}
  end

  describe "tests" do
    test "score between 0 and 5 is saved", %{conn: conn} do
      id = Repo.get_by(Product, name: "Hams Sandwich").id
      rating = %{email: "abs@beb.ee", score: "2"}
      attrs = %{rating: rating}
      conn = post(conn, Routes.product_path(conn, :update, id), rating: rating)
      conn = get conn, redirected_to(conn)
      assert html_response(conn, 200) =~ "Rating added"
    end

    test "score 6 is rejected", %{conn: conn} do
      id = Repo.get_by(Product, name: "Hams Sandwich").id
      rating = %{email: "abs@beb.ee", score: "6"}
      conn = post(conn, Routes.product_path(conn, :update, id), rating: rating)
      conn = get conn, redirected_to(conn)
      assert html_response(conn, 200) =~ "Must be between 1 and 5"
    end

    test "email is allowed to vote only once", %{conn: conn} do
      id = Repo.get_by(Product, name: "Hams Sandwich").id
      rating = %{email: "abs@beb.ee", score: "3"}
      conn = post(conn, Routes.product_path(conn, :update, id), rating: rating)
      conn = get conn, redirected_to(conn)
      assert html_response(conn, 200) =~ "Rating added"
      conn = post(conn, Routes.product_path(conn, :update, id), rating: rating)
      conn = get conn, redirected_to(conn)
      assert html_response(conn, 200) =~ "This user has already rated this product"
    end

    test "calculate average", %{conn: conn} do
      id = Repo.get_by(Product, name: "Hams Sandwich").id
      rating = %{email: "abs@beb.ee", score: "5"}
      rating2 = %{email: "abs@bebe.ee", score: "1"}
      conn = post(conn, Routes.product_path(conn, :update, id), rating: rating)
      conn = get conn, redirected_to(conn)
      conn = post(conn, Routes.product_path(conn, :update, id), rating: rating2)
      conn = get conn, redirected_to(conn)
      average = Repo.get_by(Product, name: "Hams Sandwich").average
      assert average == 3
    end
  end

end
