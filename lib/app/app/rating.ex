defmodule App.App.Rating do
  use Ecto.Schema
  import Ecto.Changeset

  schema "ratings" do
    field :email, :string
    field :score, :integer
    belongs_to :product, App.App.Product
    timestamps()
  end

  @spec changeset(
          {map, map} | %{:__struct__ => atom | %{__changeset__: map}, optional(atom) => any},
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: Ecto.Changeset.t()
  @doc false
  def changeset(rating, attrs) do
    rating
    |> cast(attrs, [:score, :email])
  end
end
