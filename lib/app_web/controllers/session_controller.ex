defmodule AppWeb.SessionController do
  use AppWeb, :controller

  alias App.Repo
  alias App.Accounts.User

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, %{"session" => %{"email" => email, "password" => password}}) do
    user = Repo.get_by(User, email: email)
    case App.Authentication.check_credentials(user, password) do
      {:ok, _} ->
        conn
        |> App.Authentication.login(user)
        |> put_flash(:info, "Welcome #{user.name}")
        |> redirect(to: Routes.page_path(conn, :index))
      {:error, _reason} ->
        conn
        |> put_flash(:error, "Bad Credentials")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> App.Authentication.logout()
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
