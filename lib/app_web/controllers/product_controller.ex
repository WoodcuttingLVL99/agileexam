defmodule AppWeb.ProductController do
  use AppWeb, :controller

  alias App.Repo
  alias App.App.Product
  alias App.App.Rating

  import Ecto.Query

  def index(conn, _params) do
    products = Repo.all(from p in Product,
                        order_by: p.average,
                        order_by: p.name)
    render conn, "index.html", products: products
  end

  def edit(conn, %{"id" => id}) do
    product = Repo.get!(Product, id)
    changeset = Rating.changeset(%Rating{}, %{})
    render(conn, "edit.html", product: product, changeset: changeset)
  end

  def update(conn, %{"product_id" => id, "rating" => rating}) do
    product = Repo.get!(Product, id)
    score = String.to_integer(rating["score"])
    cond do
      Repo.get_by(Rating, email: rating["email"]) != nil ->
        conn
        |> put_flash(:error, "This user has already rated this product")
        |> redirect(to: Routes.product_path(conn, :edit, product))
      0 < score  && score < 6 ->
        changeset = Rating.changeset(%Rating{product: product, email: rating["email"], score: score}, %{})
        Repo.insert(changeset)
        ratings = Repo.all(from r in Rating,
                           where: r.product_id == ^product.id,
                           select: r.score / 1)
        average = Enum.sum(ratings) / (Enum.count(ratings) / 1)
        App.Repo.get_by(Product, id: product.id)
        |> Ecto.Changeset.change(%{average: average})
        |> App.Repo.update()
        conn
        |> put_flash(:info, "Rating added")
        |> redirect(to: Routes.product_path(conn, :edit, product))
      true ->
        conn
        |> put_flash(:error, "Must be between 1 and 5")
        |> redirect(to: Routes.product_path(conn, :edit, product))
      end
  end

end
