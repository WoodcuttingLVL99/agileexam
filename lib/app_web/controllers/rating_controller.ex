defmodule AppWeb.RatingController do
  use AppWeb, :controller

  alias App.Repo
  alias App.App.Product
  alias App.App.Rating

  def index(conn, _params) do
    ratings = Repo.all(Rating)
    render conn, "index.html", ratings: ratings
  end

  def new(conn, %{"product" => product}) do
    changeset = Product.changeset(%Product{}, product)
    render(conn, "new.html", changeset: changeset)
  end

end
