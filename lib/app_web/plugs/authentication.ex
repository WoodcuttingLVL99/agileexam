defmodule App.Authentication do
  alias App.Guardian

  def check_credentials(user, plain_text_password) do
    if user && Pbkdf2.verify_pass(plain_text_password, user.hashed_password) do
      {:ok, user}
    else
      {:error, :unauthorized_user}
    end
  end

  def login(conn, user) do
    Guardian.Plug.sign_in(conn, user)
  end

  @spec logout(Plug.Conn.t()) :: Plug.Conn.t()
  def logout(conn) do
    Guardian.Plug.sign_out(conn)
  end

  @spec load_current_user(Plug.Conn.t()) :: any
  def load_current_user(conn) do
    Guardian.Plug.current_resource(conn)
  end

  def isAuthenticated(conn) do
    Guardian.Plug.authenticated?(conn)
  end

end
