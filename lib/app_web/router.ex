defmodule AppWeb.Router do
  use AppWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :browser_auth do
    plug App.AuthPipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AppWeb do
    pipe_through :browser
    resources "/sessions", SessionController, only: [:new, :create, :delete]
  end

  scope "/", AppWeb do
    pipe_through [:browser, :browser_auth]
    get "/", PageController, :index

    resources "/products", ProductController
    post "/products/:product_id/edit", ProductController, :edit
    post "/products/:product_id", ProductController, :update
  end

  scope "/", AppWeb do
    pipe_through [:browser]
    resources "/users", UserController, only: [:new, :create, :edit]
  end

  scope "/", AppWeb do
    pipe_through [:browser, :browser_auth, :ensure_auth]

    resources "/users", UserController
    resources "/ratings", RatingController

    # post "/search", AppController, :search
    # get "/allocations/new/:parking_lot_id", AllocationController, :custom_new
    # resources "/allocations", AllocationController, only: [:index, :create, :show, :edit, :update]
    # post "/allocations/new/:allocation_id", AllocationController, :payment
    # resources "/invoices", InvoiceController, only: [:index, :show]
  end


  # Other scopes may use custom stacks.
  # scope "/api", AppWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: AppWeb.Telemetry
    end
  end
end
