Feature: Buying products

    As a user
    I want to search for products
    So I can buy the products

    Scenario: User searches for products
        Given the products are
        | name      | cost         | stock |
        | Volvo     | 200.0        | 2     |
        | Volvo 2   | 300.0        | 5     |
        | BMW       | 300.0        | 1     |
        And the users are
        | email             | pin      | balance |
        | alf@email.com     | 123456   | 1000.0  |
        And the user logs in with "alf@email.com" and "123456"
        When the user searches "Volvo" and submits
        Then "Volvo" and "Volvo 2" are shown

    Scenario: The user buys a product
        Given the products are
        | name      | cost         | stock |
        | Volvo     | 200.0        | 2     |
        | Volvo 2   | 300.0        | 5     |
        | BMW       | 300.0        | 1     |
        And the users are
        | email             | pin      | balance |
        | alf@email.com     | 123456   | 1000.0  |
        And the user logs in with "alf@email.com" and "123456"
        When the user searches "BMW" and submits
        Then "BMW" is shown
        When the user submits buy
        Then the remaining balance is "700.0"
        And product details are shown



