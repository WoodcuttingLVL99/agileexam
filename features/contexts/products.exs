defmodule WhiteBreadContext.Products do
  use WhiteBread.Context
  use Hound.Helpers

  alias App.{Repo, App.Product, App.Rating}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end

  scenario_starting_state fn _state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(App.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(App.Repo, {:shared, self()})
  end

  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(App.Repo)
    Hound.end_session
  end

  given_ ~r/^the products are$/,
  fn state, %{table_data: table} ->
    products = table
    products |> Enum.map(fn product -> Product.changeset(%Product{}, product) end)
    |> Enum.map(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state }
  end

  and_ ~r/^the user logs in with "(?<email>[^"]+)" and "(?<pin>[^"]+)"$/,
  fn state, %{email: email, pin: pin} ->
    navigate_to "/sessions/new"
    fill_field({:id, "email"}, email)
    fill_field({:id, "password"}, pin)
    click({:id, "login_button"})
    {:ok, state }
  end

  when_ ~r/^user searches for "(?<name>[^"]+)"$/ and submits,
  fn state, %{name: name} ->
    click({:id, "products"})
    fill_field({:id, "search"}, name)
    click({:id, "search"})
    {:ok, state}
  end

  then_ ~r/^"(?<first>[^"]+)" and "(?<second>[^"]+)" are shown$/,
  fn state, %{first: first, second: second} ->
    assert visible_in_page? ~r/#{first}/
    assert visible_in_page? ~r/#{second}/
    {:ok, state}
  end

  when_ ~r/^user submits buy$/ and submits, fn state ->
    click({:id, "buy"})
    {:ok, state}
  end

  then_ ~r/^the remaining balance is "(?<balance>[^"]+)"$/,
  fn state, %{balance: balance} ->
    assert visible_in_page? ~r/#{balance}/
    {:ok, state}
  end

  and_ ~r/^the product details are shown "(?<name>[^"]+)", "(?<cost>[^"]+)", "(?<stock>[^"]+)"$/,
  fn state, %{name: name, cost: cost, stock: stock} ->
    assert visible_in_page? ~r/#{name}/
    assert visible_in_page? ~r/#{cost}/
    assert visible_in_page? ~r/#{stock}/
    {:ok, state}
  end

end
