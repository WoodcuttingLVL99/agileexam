defmodule WhiteBreadConfig do
  use WhiteBread.SuiteConfiguration

  suite name:          "Products",
        context:       WhiteBreadContext.Products,
        feature_paths: ["features/scenarios/searching_buying_products/"]

end
